const express = require('express');
const mongoose = require('mongoose');
const app = express();
const port =process.env.PORT || 5000; 

require('dotenv/config'); 


//app.use(cors());
app.use(express.json());

const uri = process.env.ATLAS_URI

mongoose.connect(uri, {useUnifiedTopology: true,useNewUrlParser: true}
  );

  const connection=mongoose.connection;
connection.once('open',()=>{
  console.log('DB Connected!'); 
})

const exercisesRouter=require('./routes/exercises');
const usersRouter=require('./routes/users');

app.use('/exercises',exercisesRouter);
app.use('/users',usersRouter);

app.listen(port,() => {
    console.log(`Your port is ${port}`);
  });
